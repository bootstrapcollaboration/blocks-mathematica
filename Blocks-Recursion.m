(*Setup*)

prec=200;
half=SetPrecision[1/2,prec];

highPrecisionToString[x_] := Module[
    {digits = 32, padding = 10, s},
    s = StringTrim[
        StringInsert[
            StringJoin[
                Sequence[Table["0", {i, digits + 1}]],
                ToString[
                    Quotient[Round[10^(digits + padding) Abs[x]], 10^padding]]],
            ".", -(digits + 1)],
        "0" ..];
    If[s == ".", "0",
       StringJoin[
           If[x < 0, "-", ""], 
           If[StringTake[s, 1] == ".",
              StringJoin["0", s],
              s]]]];

zzbDerivTablePath[tableDir_, d_, delta12_, delta34_, L_, nmax_, keptPoleOrder_, order_] :=
    StringJoin[tableDir,
               "/nmax",            ToString[nmax],
               "/zzbDerivTable-d", highPrecisionToString[d],
               "-delta12-",        highPrecisionToString[delta12],
               "-delta34-",        highPrecisionToString[delta34],
               "-L",               ToString[L],
               "-nmax",            ToString[nmax],
               "-keptPoleOrder",   ToString[keptPoleOrder],
               "-order",           ToString[order], ".m"];

safeExport[file_, expr_] := Module[
    {dir = DirectoryName[file]},
    If[!DirectoryQ[dir],
       If[ $OperatingSystem == "Unix",
           Run["mkdir -p -m 777 "<>dir];,
           CreateDirectory[dir,CreateIntermediateDirectories->True];
       ];
    ];
    Export[file, expr];
    If[ $OperatingSystem == "Unix",
        Run["chmod a+rw "<>file];
    ];
];

(* The value of r at the crossing symmetric point *)
rCrossing=SetPrecision[3-2Sqrt[2],prec];

(* Transpose a k by n matrix where k might be zero *)
transposeSafe[{},n_]:=Table[{},{i,n}];
transposeSafe[l_,n_]:=Transpose[l];

(* zip two lists together *)
zip[xs_,ys_]:=Transpose[{xs,ys}];

(* Derivatives with respect to r and \[Eta] (respectively) corresponding to nmax. Note that Max[m] was here shifted to 2nmax-1 rather than 2nmax compared to the earlier file. *)
mnDerivSet[nmax_]:=Table[{m,0},{m,0,2 nmax - 1}];

(* Turn a vector of derivatives into a set of rules of the form rcDeriv[m,n]\[Rule]x *)
vectorToRules[v_,nmax_]:=Rule@@#&/@zip[rcDeriv@@#&/@mnDerivSet[nmax],v];
(* Transform back from a set of rules to a vector *)
rulesToVector[rules_,nmax_]:=(rcDeriv@@#&/@mnDerivSet[nmax])/.rules;

(* Evaluate a partial fraction at Delta=a *)
(*evalPartialFraction[PartialFraction[pol_,rs_],Delta_,a_]:=(pol/.Delta->a)+Sum[r[[2]]/(a-r[[1]]),{r,rs}]*)
(*evalPartialFraction[PartialFraction[pol_,rs_],Delta_,a_]:=(pol/.Delta->a)+Table[1/(a-r[[1]]),{r,rs}].(Last/@rs);*)
evalPartialFraction[PartialFraction[pol_,rs_],Delta_,a_]:=(pol/.Delta->a)+(1/(a-(First/@rs))).(Last/@rs);
(* Do the replacement Delta\[Rule]Delta+a in a partial fraction *)
shiftPartialFraction[PartialFraction[pol_,rs_],Delta_,a_]:=PartialFraction[pol/.Delta->Delta+a,Table[{r[[1]]-a,r[[2]]},{r,rs}]];

(* Computes the coefficient of x^m in a series obtained by Series[f[x],{x,0,k}] *)
seriesCoefficient[HoldPattern[SeriesData[_,_,coeffs_,nmin_,nmax_,_]],m_]:=Module[
{i=m-nmin+1},
If[i<=0,0,coeffs[[m-nmin+1]]]
];

(* ::Subtitle:: *)
(*Recursion*)


(* A matrix representing the action of multiplication by r^Delta on a set of (r,\[Eta]) derivatives, with an overall factor of r^Delta stripped off. *)
rDeltaMatrix[nmax_]:=Module[{exprs,basis},
exprs=Table[FunctionExpand[r^-Delta D[r^Delta f[r],{r,m}]],{m,0,2nmax-1}];
basis=Table[D[ f[r],{r,m}],{m,0,2nmax-1}];
CoefficientArrays[exprs,basis][[2]]
];

(* To get g from h, we must multiply by r^Delta.  This function performs that transformation on the PoleSeries of derivatives, resulting in a list of PartialFractions. (Derivatives of r^Delta result in polynomials in Delta, which is why we have PartialFractions.) *)
restoreRDelta[PartialFraction[const_,poles_],nmax_,r0_]:=Module[{rDeltaM,pols,poleProducts},
rDeltaM=Normal[rDeltaMatrix[nmax]]/.r->r0;
poleProducts=Table[
PolynomialQuotientRemainder[#,Delta-p[[1]],Delta]&/@(rDeltaM.p[[2]])
,{p,poles}
];
pols=rDeltaM.const+Total[First/@#&/@poleProducts];
Thread[PartialFraction[Expand[pols],zip[First/@poles,#]&/@transposeSafe[Last/@#&/@poleProducts,Length[pols]]]]
];


computeBlocks[LMax_,nu_,Delta12_,Delta34_,nmax_,order_]:=Module[
{residues,LMaxRecursion,maxPoleOrder,newResidues,residueTypes,residueL,residueDelta,residueShift,alpha,rType,rShift,rL,rDelta,ralpha,Deltavector,hInfinity,coefficientsToDerivsAroundRho,consts,poles},

LMaxRecursion=LMax+order;
maxPoleOrder[L_]:=Min[order,LMaxRecursion-L];

residueTypes[L_]:=residueTypes[L]=Join[
Table[{1,n},{n,1,maxPoleOrder[L]}],
Table[{2,n},{n,2,maxPoleOrder[L],2}],
Table[{3,n},{n,1,Min[maxPoleOrder[L],L]}]
];

residues=Table[
ConstantArray[0,{order+1,Length[residueTypes[L]]}],
{L,0,LMaxRecursion}
];

hInfinity=PadRight[Series[(1-r)^(-1+Delta12-Delta34-nu) (1+r)^(-1-Delta12+Delta34-nu),{r,0,order}][[3]],order+1];

alpha[L_,{1,n_}]:=alpha[L,{1,n}]=-((4^n n)/(n!)^2) Pochhammer[1/2 (1-n+Delta12),n] Pochhammer[1/2 (1-n+Delta34),n] Pochhammer[2nu+L,n]/Pochhammer[nu+L,n];
alpha[L_,{2,n_}]:=alpha[L,{2,n}]=If[OddQ[n],0,-(4^n n (-1)^(n/2) Pochhammer[-(n/2)+nu,n] Pochhammer[1/2 (1-n/2+L-Delta12+nu),n/2] Pochhammer[1/2 (1-n/2+L+Delta12+nu),n/2] Pochhammer[1/2 (1-n/2+L-Delta34+nu),n/2] Pochhammer[1/2 (1-n/2+L+Delta34+nu),n/2])/(2 (((n/2)!)^2 Pochhammer[-(n/2)+L+nu,n] Pochhammer[1-n/2+L+nu,n]))];
alpha[L_,{3,n_}]:=alpha[L,{3,n}]= -((4^n n)/(n!)^2) Pochhammer[1/2 (1-n+Delta12),n] Pochhammer[1/2 (1-n+Delta34),n] Pochhammer[1+L-n,n]/Pochhammer[1+nu+L-n,n];

residueL[L_,{1,n_}]:=L+n;
residueL[L_,{2,k_}]:=L;
residueL[L_,{3,n_}]:=L-n;

residueDelta[L_,{1,n_}]:=1-L-n;
residueDelta[L_,{2,n_}]:=1+nu-n/2;
residueDelta[L_,{3,n_}]:=L+2nu+1-n;

residueShift[_,{_,n_}]:=n;

WriteString["stdout","\nPerforming recursion."];
WriteString["stdout","\nRecursion order: "];
Do[
WriteString["stdout",p,", "];
Do[
Do[
rType=residueTypes[L][[n]];
rShift=residueShift[L,rType];
If[rShift<=maxPoleOrder[L]&&rShift<=p,
rL=residueL[L,rType];
rDelta=residueDelta[L,rType]+rShift;
ralpha=alpha[L,rType];
Deltavector=Table[1/(rDelta-residueDelta[rL,rT]),{rT,residueTypes[rL]}];
residues[[L+1,p+1,n]]=ralpha(hInfinity[[p-rShift+1]]+residues[[rL+1,p-rShift+1]].Deltavector);
];
,
{n,Length[residueTypes[L]]}
],
{L,0,LMaxRecursion-p+1}
];,
{p,order}
];

WriteString["stdout","\nEvaluating derivatives using series expansion."];
coefficientsToDerivsAroundRho=Table[If[m>=n,m!/(m-n)! rCrossing^(m-n),0],{n,0,2nmax-1},{m,0,order}];
consts=coefficientsToDerivsAroundRho.hInfinity;
Table[
poles=Transpose[coefficientsToDerivsAroundRho.residues[[L+1]]];
restoreRDelta[PartialFraction[consts,Table[{residueDelta[L,residueTypes[L][[m]]],poles[[m]]},{m,Length[residueTypes[L]]}]],nmax,rCrossing],
{L,0,LMax}
]
];


(* ::Subtitle:: *)
(*Polynomial Truncation*)


(* This version of truncation attempts to approximate discarded poles in terms of kept poles.  The approximation is exact at x=0 (the unitarity bound for spin l \[GreaterEqual] 1, and at x=Infinity, and in general is better by a factor of 10^-2 or 10^-3 than naively discarding a pole. *)
(* We must make sure the 'to' poles are not exact numbers -- otherwise we get major memory usage! *)
shiftPoles[from_,to_]:=Module[{n=Length[to]},
zip[to,LinearSolve[
Table[SetPrecision[to[[i]],prec]^m,{m,-Floor[n/2],n-1-Floor[n/2]},{i,n}],
Table[Sum[p[[2]]SetPrecision[p[[1]],prec]^m,{p,from}],{m,-Floor[n/2],n-1-Floor[n/2]}]
]]
];

togetherWithFactors[factors_,x_][PartialFraction[poly_,rs_]]:=Module[{mult},
mult=Product[x-f,{f,factors}];
N[poly*mult+Sum[r[[2]]*Product[x-f,{f,Complement[factors,{r[[1]]}]}],{r,rs}],prec]//Expand
];

(* If a pole p satisfies protectPole[p]=True, leave its residue untouched (neither shift the pole elsewhere, nor shift other poles to it), meanwhile shift all other poles in the given partial fraction to polesToKeep *)
polynomialTruncate[PartialFraction[polynomial_,polePart_],protectPole_,polesToKeep_,x_]:=Module[
{protected,unprotected,keep,allKeptPoles},
protected=Select[polePart,protectPole[First[#]]&];
unprotected=Select[polePart,!protectPole[First[#]]&];
keep=Select[polesToKeep,!protectPole[#]&];
allKeptPoles=Join[protected,shiftPoles[unprotected,keep]];
togetherWithFactors[First/@allKeptPoles,x][PartialFraction[polynomial, allKeptPoles]]
];
(* The location of the poles in Delta in a conformal block, where order specifies how many poles *)
cbPoles[nu_,L_,order_]:=Join[
Table[-(n+L-1),{n,order}],
Table[-(-nu+k-1),{k,(order)/2}],
Table[-(-L-2nu+n-1),{n,Min[order,L]}]
];
(* Compute polynomial approximations for the components of a conformal block, where we keep poles up to the order keptPoleOrder.  The resulting blocks are shifted over by L+2nu. We protect the pole at the unitarity bound, so that its residue remains exact. *)
blockPolynomials[block_,nu_,L_,keptPoleOrder_]:=Module[{
keep=#-(L+2nu)&/@cbPoles[nu,L,keptPoleOrder],
protect=Function[p,p==If[L==0,-nu,0]]
},
polynomialTruncate[shiftPartialFraction[#,Delta,L+2nu],protect,keep,Delta]&/@block
];
(* compute blockPolynomials for each spin L *)
blockTablePolynomialsShifted[blocks_,nu_,keptPoleOrder_]:=Table[
blockPolynomials[blocks[[L+1]],nu,L,keptPoleOrder]/.Delta->x,
{L,0,Length[blocks]-1}
];


(* ::Subtitle:: *)
(*Casimir Equation Recursion*)


(* lambda = Delta (Delta - d) + L (L + d - 2) *)
casimirRecursionTable[d_,Delta12_,Delta34_,nmax_]:=Module[{CR},
CR[m_,n_/;n<0]:=0;
CR[m_/;m<0,n_]:=0;
CR[m_,0]:=aDeriv[m];
CR[m_,n_]:=CR[m,n]=Expand[
m (2-3 m+m^2) CR[-3+m,n]+(-1+m) m CR[-2+m,n]+(1/(2 (-3+d+2 n)))m (20+m^2+d (1+m-2 n)-30 n+12 n^2+5 Delta12-4 n Delta12-5 Delta34+4 n Delta34-Delta12 Delta34+m (-15+12 n-Delta12+Delta34)) CR[-1+m,-1+n]-m CR[-1+m,n]+(1/(2 (-3+d+2 n)))(2+m^2-6 n+4 n^2+2 d (-1+m+n)+4 Delta12-4 n Delta12-4 Delta34+4 n Delta34-Delta12 Delta34+m (-9+8 n-2 Delta12+2 Delta34)+2 lambda) CR[m,-1+n]+((-1+n) (-4-d+3 m+4 n-Delta12+Delta34) CR[1+m,-2+n])/(2 (-3+d+2 n))+((4+d-m-4 n-Delta12+Delta34) CR[1+m,-1+n])/(2 (-3+d+2 n))+((-1+n) CR[2+m,-2+n])/(2 (-3+d+2 n))+CR[2+m,-1+n]/(6-2 d-4 n)
];
Flatten[Table[abDeriv[m,n]->CR[m,n],{n,0,2nmax-1},{m,0,2nmax-1-n}],1]
];

zzbToABTable[nmax_]:=Flatten[Table[
zzbDeriv[j,k]->Sum[
((-1)^(j-m) abDeriv[j+k-2 n,n] j! k! (2 n)!)/((j-m)! m! (j+k-m-2 n)! n! (-j+m+2 n)!),
{n,0,(j+k)/2},
{m,0,j+k-2n}
],
{j,0,2nmax-1},
{k,0,Min[2nmax-1-j,j]}
],1];

rho[z_]:=z/(1+Sqrt[1-z])^2;
lambda[z_]:=Sqrt[rho[z]/rho[1/2]];
Clear[lambdaSeries];
lambdaSeries[order_]:=lambdaSeries[order]=Series[lambda[z],{z,half,order}];
Clear[alphaSeries];
alphaSeries[p_,order_]:=alphaSeries[p,order]=If[
p==0,
Prepend[ConstantArray[0,order],1],
(lambdaSeries[order]^p)[[3]]
];

aDerivExpr[n_]:=Module[{alpha},
alpha[q_,p_]:=q!alphaSeries[p,n][[q+1]];
Sum[
2^-n rcDeriv[k,0]/k! rho[half]^k Sum[(-1)^(k-j) Binomial[k,j]alpha[n,2j],{j,0,k}],
{k,0,n}
]
];
aToRcTable[nMax_]:=Table[aDeriv[n]->aDerivExpr[n],{n,0,4nMax}];

zzbToRcCasimirTable[d_,Delta12_,Delta34_,nMax_]:=(
  zzbToABTable[nMax]/.casimirRecursionTable[d,Delta12,Delta34,nMax]//Collect[#,aDeriv[_],Expand]&
  )/.aToRcTable[nMax]//Collect[#,rcDeriv[_,_],Expand]&;

(* ::Subtitle:: *)
(*Outputting Tables*)

reportMemory[msg_] := WriteString["stdout",
                                  "\n- memory in use: ", MemoryInUse[]/2^30., "GB (", msg, ")",
                                  "\n- max memory used: ", MaxMemoryUsed[]/2^30., "GB (", msg, ")"];

(* Accuracy of blocks goes like 10^(-1.3*keptPoleOrder) *)
(* Correctness of coefficients in polynomial truncations goes like 10^(-0.67*order) *)
(* Precision of numbers goes like 10^(-0.5*keptPoleOrder) *)
WriteZZbDerivTable[tableDir_, dim_, delta12LowPrecision_, delta34LowPrecision_, spins_, nmax_, keptPoleOrders_, order_] := Module[
    {
        Lmax = Max[spins],
        blocksOnDiagonal, shiftedblocksPols, casimirRules, file,
        d = Rationalize[dim,10^-6],
        delta12 = SetPrecision[delta12LowPrecision, prec],
        delta34 = SetPrecision[delta34LowPrecision, prec]
    },
    reportMemory["start"];
    WriteString["stdout", "\nComputing blocks on the diagonal."];
    Print[Timing[blocksOnDiagonal = computeBlocks[Lmax, (d-2)/2, delta12, delta34, nmax, order];]];
    reportMemory["after diagonal"];

    WriteString["stdout", "\nUsing casimir to compute off-diagonal derivatives."];
    Print[Timing[casimirRules = zzbToRcCasimirTable[d, delta12, delta34, nmax];]];
    reportMemory["after casimir rules"];

    Do[
        WriteString["stdout", "\nDoing polynomial truncation: keptPoleOrder = ", keptPoleOrder];
        Print[Timing[shiftedblocksPols=blockTablePolynomialsShifted[blocksOnDiagonal, (d-2)/2, keptPoleOrder];]];
        reportMemory["after pol truncation"];

        Do[
            file = zzbDerivTablePath[tableDir, d, delta12, delta34, L, nmax, keptPoleOrder, order];
            WriteString["stdout", "\nWriting file: ", file, " ... "];
            Print[Timing[safeExport[
                file,
                ((casimirRules
                  /. lambda -> L*(-2+d+L)+(-2+L+x)*(-2+d+L+x))
                 /. vectorToRules[shiftedblocksPols[[L+1]],nmax]) // Expand];]];
            ,
            {L,spins}];,
        {keptPoleOrder,keptPoleOrders}];
];
