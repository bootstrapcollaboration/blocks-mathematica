This repository contains mathematica code for generating tables of
derivatives of conformal blocks. The blocks are given as polynomials
in x, where x = Delta - (d-2) (Note that for scalars, x=0 is *not* the
unitarity bound!). I believe the code is written for general spacetime
dimension d, but I have only tested it for d=3. The code is not
well-documented.